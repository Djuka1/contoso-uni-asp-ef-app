# ASP.NET Core MVC with EntityFramework App
## Contoso University

This is a sample web application that demonstrates how to create ASP.NET Core 2.0 MVC web applications using Entity Framework Core 2.0. <br/>
This is a web site for a fictional university named Contoso University.<br/> 
It includes functionalities as:
- Student admission
- Course creation
- Instructor assignments

This application is built from scratch.