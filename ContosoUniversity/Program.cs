﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using ContosoUniversity.Data;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace ContosoUniversity
{
    public class Program
    {
        public static void Main(string[] args)
        {
			IWebHost host = BuildWebHost(args);
            using(IServiceScope scope = host.Services.CreateScope())
			{
				IServiceProvider services = scope.ServiceProvider;
				try
				{
					SchoolContext context = services.GetRequiredService<SchoolContext>();
					DbInitializer.Initialize(context);
				}
				catch (Exception ex)
				{
					Logger<Program> logger = services.GetRequiredService<Logger<Program>>();
					logger.LogError(ex, "An error occured while seeding the database.");
				}
			}

			host.Run();

			// BuildWebHost(args).Run();
        }

        public static IWebHost BuildWebHost(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>()
                .Build();
    }
}
